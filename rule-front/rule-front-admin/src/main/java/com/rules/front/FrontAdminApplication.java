package com.rules.front;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.rules")
@MapperScan(basePackages = "com.rules.*.dao")
@EnableMethodCache(basePackages = "com.rules")
@EnableCreateCacheAnnotation
public class FrontAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontAdminApplication.class, args);
    }
}
