package com.rules.front.api.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rules.admin.entity.DictEntity;
import com.rules.admin.service.DictService;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 数据字典表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-04-30 19:55:59
 */
@RestController
@RequestMapping("sys/dict")
public class DictController {
    @Autowired
    private DictService dictService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = dictService.queryPage(params);
        return Result.ok().put("page",page);
    }

    /**
     * 列表
     */
    @RequestMapping("/qyType")
    public Result qyType(@RequestParam Map<String, Object> params){
        String key=String.valueOf(params.get("key"));
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.in("type",key.split(","));
        List<DictEntity> matchsList = dictService.list(queryWrapper);
        Map<String,List<DictEntity>> mapmatchsListMap=matchsList.stream()
                .collect(Collectors.groupingBy(DictEntity::getType));
        return Result.ok().put("dict",mapmatchsListMap);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Result info(@PathVariable("id") Long id){
		DictEntity dict = dictService.getById(id);

        return Result.ok().put("dict",dict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody DictEntity dict){
		dictService.save(dict);

        return Result.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody DictEntity dict){
		dictService.updateById(dict);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] ids){
		dictService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

}
