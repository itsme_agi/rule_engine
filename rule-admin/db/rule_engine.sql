/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.98.53
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 192.168.98.53:3306
 Source Schema         : facebook_rule

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 14/05/2020 11:23:13
*/
drop database  if exists rule_engine;
CREATE DATABASE rule_engine CHARSET utf8mb4;

use `rule_engine`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rule_action_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_action_info`;
CREATE TABLE `rule_action_info`  (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_type` int(11) NOT NULL COMMENT '动作类型(1实现2自身)',
  `action_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '动作名称',
  `action_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '动作描述',
  `action_class` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '动作实现类(包路径)',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`action_id`) USING BTREE,
  INDEX `action_type`(`action_type`) USING BTREE,
  INDEX `action_name`(`action_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则动作定义信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_action_info
-- ----------------------------
INSERT INTO `rule_action_info` VALUES (1, 1, '促销活动实现类', '促销活动实现类', 'com.rules.drools.service.impl.TestActionImpl', 1, 1, '2017-07-24 17:12:32', '促销活动实现类');
INSERT INTO `rule_action_info` VALUES (2, 2, '自身', '测试自身', 'com.rules.drools.vo.TestRule', 1, 1, '2017-07-27 10:07:03', '测试自身');
INSERT INTO `rule_action_info` VALUES (4, 2, '订单规则', '订单规则', 'com.rules.drools.vo.OrderRule', 1, NULL, '2019-06-05 15:26:30', '订单规则');

-- ----------------------------
-- Table structure for rule_action_param_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_action_param_info`;
CREATE TABLE `rule_action_param_info`  (
  `action_param_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` bigint(20) NOT NULL COMMENT '动作id',
  `action_param_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数名称',
  `action_param_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数描述',
  `param_identify` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标识',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`action_param_id`) USING BTREE,
  INDEX `action_id`(`action_id`) USING BTREE,
  INDEX `action_param_name`(`action_param_name`) USING BTREE,
  INDEX `param_identify`(`param_identify`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '动作参数信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_action_param_info
-- ----------------------------
INSERT INTO `rule_action_param_info` VALUES (1, 1, '促销活动实现类测试', '测试', 'message', 1, 1, '2017-07-24 18:24:28', NULL);
INSERT INTO `rule_action_param_info` VALUES (2, 2, '自身测试', '测试', 'score', 1, 1, '2017-07-17 10:07:46', NULL);
INSERT INTO `rule_action_param_info` VALUES (7, 4, '会员当前订单产生的积分', '会员当前订单产生的积分', 'nowScore', 1, NULL, '2019-06-05 15:29:17', '会员当前订单产生的积分');

-- ----------------------------
-- Table structure for rule_action_param_value_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_action_param_value_info`;
CREATE TABLE `rule_action_param_value_info`  (
  `action_param_value_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rule_action_rel_id` bigint(20) NOT NULL COMMENT '动作规则关系主键',
  `action_param_id` bigint(20) NOT NULL COMMENT '动作参数',
  `param_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数值',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`action_param_value_id`) USING BTREE,
  INDEX `rule_action_rel_id`(`rule_action_rel_id`) USING BTREE,
  INDEX `action_param_id`(`action_param_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '动作参数对应的属性值信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_action_param_value_info
-- ----------------------------
INSERT INTO `rule_action_param_value_info` VALUES (1, 4, 1, 'hello', 1, 1, '2017-07-24 19:29:17', NULL);
INSERT INTO `rule_action_param_value_info` VALUES (2, 5, 2, '#3#*5', 1, 1, '2017-07-27 10:10:17', NULL);
INSERT INTO `rule_action_param_value_info` VALUES (8, 3, 7, '#6#*0.8', 1, NULL, '2019-06-05 16:18:34', '');

-- ----------------------------
-- Table structure for rule_action_rule_rel
-- ----------------------------
DROP TABLE IF EXISTS `rule_action_rule_rel`;
CREATE TABLE `rule_action_rule_rel`  (
  `rule_action_rel_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` bigint(20) NOT NULL COMMENT '动作',
  `rule_id` bigint(20) NOT NULL COMMENT '规则',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `rule_action_rel_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`rule_action_rel_id`) USING BTREE,
  INDEX `action_id`(`action_id`) USING BTREE,
  INDEX `rule_id`(`rule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '动作与规则信息关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_action_rule_rel
-- ----------------------------
INSERT INTO `rule_action_rule_rel` VALUES (1, 1, 1, 1, '促销活动实现类测试', 1, '2017-07-24 18:59:11', '促销活动实现类测试');
INSERT INTO `rule_action_rule_rel` VALUES (2, 2, 1, 1, '自身测试', 1, '2017-07-27 10:08:25', '自身');
INSERT INTO `rule_action_rule_rel` VALUES (3, 4, 2, 1, '订单积分规则', NULL, '2019-06-05 15:34:52', '');
INSERT INTO `rule_action_rule_rel` VALUES (4, 1, 3, 1, NULL, NULL, '2019-11-20 17:34:49', NULL);
INSERT INTO `rule_action_rule_rel` VALUES (5, 2, 4, 1, NULL, NULL, '2019-11-20 17:35:56', NULL);

-- ----------------------------
-- Table structure for rule_condition_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_condition_info`;
CREATE TABLE `rule_condition_info`  (
  `condition_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rule_id` bigint(20) NOT NULL COMMENT '规则',
  `condition_name` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '条件名称',
  `condition_expression` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '条件表达式',
  `condition_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '条件描述',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`condition_id`) USING BTREE,
  INDEX `rule_id`(`rule_id`) USING BTREE,
  INDEX `condition_name`(`condition_name`(255)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则条件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_condition_info
-- ----------------------------
INSERT INTO `rule_condition_info` VALUES (1, 1, '测试', '$2$==100', '$金额$==100', 1, 1, '2017-07-24 20:04:52', NULL);
INSERT INTO `rule_condition_info` VALUES (2, 2, 'orderScore<500', '$6$<500', '订单金额小于500', 1, NULL, '2019-06-05 15:08:04', '');

-- ----------------------------
-- Table structure for rule_entity_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_entity_info`;
CREATE TABLE `rule_entity_info`  (
  `entity_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `entity_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `entity_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `entity_identify` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标识',
  `pkg_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '包路径',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效(1是0否)',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`entity_id`) USING BTREE,
  INDEX `entity_identify`(`entity_identify`) USING BTREE,
  INDEX `entity_name`(`entity_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则引擎实体信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_entity_info
-- ----------------------------
INSERT INTO `rule_entity_info` VALUES (1, '测试规则', '测试规则引擎', 'testRule', 'com.rules.drools.vo.TestRule', 1, '2017-07-20 11:41:32', 1, NULL);
INSERT INTO `rule_entity_info` VALUES (2, '订单积分', '订单积分实体', 'orderRule', 'com.rules.drools.vo.OrderRule', NULL, '2019-06-05 14:57:57', 1, '');

-- ----------------------------
-- Table structure for rule_entity_item_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_entity_item_info`;
CREATE TABLE `rule_entity_item_info`  (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `entity_id` bigint(20) NOT NULL COMMENT '实体id',
  `item_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段名称',
  `item_identify` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段标识',
  `item_desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '属性描述',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`item_id`) USING BTREE,
  INDEX `entity_id`(`entity_id`) USING BTREE,
  INDEX `item_identify`(`item_identify`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体属性信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_entity_item_info
-- ----------------------------
INSERT INTO `rule_entity_item_info` VALUES (1, 1, '消息', 'message', '消息信息', 1, '2017-07-20 16:20:56', 1, NULL);
INSERT INTO `rule_entity_item_info` VALUES (2, 1, '金额', 'amount', '消费金额', 1, '2017-07-20 16:21:47', 1, NULL);
INSERT INTO `rule_entity_item_info` VALUES (3, 1, '积分', 'score', '积分', 1, '2017-07-20 16:23:55', 1, NULL);
INSERT INTO `rule_entity_item_info` VALUES (4, 2, '订单id', 'orderId', '', NULL, '2019-06-05 15:00:46', 1, '');
INSERT INTO `rule_entity_item_info` VALUES (5, 2, '会员id', 'memberId', '', NULL, '2019-06-05 15:01:15', 1, '');
INSERT INTO `rule_entity_item_info` VALUES (6, 2, '订单金额', 'orderMoney', '', NULL, '2019-06-05 15:01:40', 1, '');
INSERT INTO `rule_entity_item_info` VALUES (7, 2, '会员已有总积分', 'beforeScore', '', NULL, '2019-06-05 15:02:11', 1, '');
INSERT INTO `rule_entity_item_info` VALUES (8, 2, '当前订单计算后的总积分', 'afterScore', '', NULL, '2019-06-05 15:02:26', 1, '');
INSERT INTO `rule_entity_item_info` VALUES (9, 2, '会员当前订单产生的积分', 'nowScore', '', NULL, '2019-06-05 15:02:38', 1, '');

-- ----------------------------
-- Table structure for rule_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_info`;
CREATE TABLE `rule_info`  (
  `rule_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `scene_id` bigint(20) NOT NULL COMMENT '场景',
  `rule_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `rule_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `rule_enabled` int(11) NOT NULL DEFAULT 1 COMMENT '是否启用',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`rule_id`) USING BTREE,
  INDEX `scene_id`(`scene_id`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_info
-- ----------------------------
INSERT INTO `rule_info` VALUES (1, 1, '测试', NULL, 1, 1, 1, '2017-07-25 10:55:36', NULL);
INSERT INTO `rule_info` VALUES (2, 2, 'orderScore1', '订单积分规则1：订单金额小于500', 1, 1, NULL, '2019-06-05 15:06:00', '');

-- ----------------------------
-- Table structure for rule_property_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_property_info`;
CREATE TABLE `rule_property_info`  (
  `rule_property_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rule_property_identify` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标识',
  `rule_property_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `rule_property_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `default_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '默认值',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`rule_property_id`) USING BTREE,
  INDEX `rule_property_identify`(`rule_property_identify`) USING BTREE,
  INDEX `rule_property_name`(`rule_property_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则基础属性信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_property_info
-- ----------------------------
INSERT INTO `rule_property_info` VALUES (1, 'salience', '优先级', '用来设置规则执行的优先级，salience 属性的值是一个数字，数字越大执行优先级越高，同时它的值可以是一个负数。默认情况下，规则的ssalience 默认值为0，所以如果我们不手动设置规则的salience 属性，那么它的执行顺序是随机（但是一般都是按照加载顺序。）', '0', 1, NULL);
INSERT INTO `rule_property_info` VALUES (2, 'no-loop', '是否可被重复执行', '是否允许规则多次执行, 值为布尔类型, 默认是false, 即当前的规则只要满足条件, 可以无限次执行; 对当前传入workingmemory中的fact对象进行修改或者个数的增减时, 就会触发规则重新匹配执行; 设置属性no-loop true, 表示当前规则只执行一次, 即使rhs中更新了当前fact对象也不会再次执行该规则了. 不过当其他规则里更新了fact对象时, 即使有no-loop true也会触发, 即no-loop true仅表示本规的rhs中有更新时不重复执行', 'false', 1, NULL);
INSERT INTO `rule_property_info` VALUES (3, 'date-effective', '生效时间', '设置规则的生效时间, 当前系统时间>=date-effective时才会触发执行, 值是一个日期格式的字符串, 推荐用法是手动在java代码中设置当前系统的时间格式, 然后按照格式指定时间. 比如: date-effective \"2016-01-31 23:59:59\"', NULL, 1, NULL);
INSERT INTO `rule_property_info` VALUES (4, 'date-expires', '失效时间', '设置规则的失效时间, 跟生效时间正好相反', NULL, 1, NULL);
INSERT INTO `rule_property_info` VALUES (5, 'enabled', '是否可用', '表示该规则是否可用, 值为布尔类型, 默认是true, 设置成false则该规则就不会被执行了', 'true', 0, '规则信息上已经有此属性，所以默认当前属性无效');
INSERT INTO `rule_property_info` VALUES (6, 'dialect', '语言类型', '设置语言类型, 值为字符串, 一般有两种语言,mvel和java, 默认为java', 'java', 1, NULL);
INSERT INTO `rule_property_info` VALUES (7, 'duration', '规则定时', '规则定时, 值为长整型, 单位为毫秒, 如 duration 3000, 表示规则在3秒后执行(另外的线程中执行)', '3000', 1, NULL);
INSERT INTO `rule_property_info` VALUES (8, 'lock-on-active', '确认规则只执行一次', '是no-loop的增强版, 与其他属性配合使用;规则的重复执行不一定是本身触发的, 也可能是其他规则触发的, 当在规则上使用ruleflow-group属性或agenda-group属性时, 将lock-on-active属性值设置为true，可避免因某些fact对象被修改而使已经执行过的规则再次被激活执行', 'false', 1, NULL);
INSERT INTO `rule_property_info` VALUES (9, 'activation-group', '规则分组', '作用是将规则分组, 值为字符串表示组名,这样在执行的时候,具有相同activation-group属性的规则中只要有一个会被执行,其它的规则都将不再执行。即在具有相同activation-group属性的规则当中,只有一个规则会被执行,其它规则都将不会被执行.相同activation-group属性的规则中哪一个会先执行,则可以用salience之类的属性来实现', NULL, 1, NULL);
INSERT INTO `rule_property_info` VALUES (10, 'agenda-group', '议程分组', 'agenda group 是用来在agenda 的基础之上，对现在的规则进行再次分组，具体的分组方法可以采用为规则添加agenda-group 属性来实现。 \r\nagenda-group 属性的值也是一个字符串，通过这个字符串，可以将规则分为若干个agenda group，默认情况下，引擎在调用这些设置了agenda-group 属性的规则的时候需要显示的指定某个agenda group 得到focus（焦点），这样位于该agenda group 当中的规则才会触发执行，否则将不执行', NULL, 1, NULL);
INSERT INTO `rule_property_info` VALUES (11, 'ruleflow-group', '规则流分组', '在使用规则流的时候要用到ruleflow-group 属性，该属性的值为一个字符串，作用是用来将规则划分为一个个的组，然后在规则流当中通过使用ruleflow-group 属性的值，从而使用对应的规则', NULL, 1, NULL);
INSERT INTO `rule_property_info` VALUES (12, 'auto-focus', '自动获取焦点', '前面我们也提到auto-focus 属性，它的作用是用来在已设置了agenda-group 的规则上设置该规则是否可以自动独取focus，如果该属性设置为true，那么在引擎执行时，就不需要显示的为某个agenda group 设置focus，否则需要', 'true', 1, NULL);

-- ----------------------------
-- Table structure for rule_property_rel
-- ----------------------------
DROP TABLE IF EXISTS `rule_property_rel`;
CREATE TABLE `rule_property_rel`  (
  `rule_pro_rel_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rule_id` bigint(20) NOT NULL COMMENT '规则',
  `rule_property_id` bigint(20) NOT NULL COMMENT '规则属性',
  `rule_property_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则属性值',
  PRIMARY KEY (`rule_pro_rel_id`) USING BTREE,
  INDEX `rule_id`(`rule_id`) USING BTREE,
  INDEX `rule_property_id`(`rule_property_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则属性配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_property_rel
-- ----------------------------
INSERT INTO `rule_property_rel` VALUES (1, 1, 1, '1');
INSERT INTO `rule_property_rel` VALUES (2, 1, 2, 'true');
INSERT INTO `rule_property_rel` VALUES (3, 1, 8, 'true');
INSERT INTO `rule_property_rel` VALUES (4, 2, 1, '1');
INSERT INTO `rule_property_rel` VALUES (5, 2, 2, 'true');
INSERT INTO `rule_property_rel` VALUES (6, 2, 8, 'true');
INSERT INTO `rule_property_rel` VALUES (7, 3, 1, '2');
INSERT INTO `rule_property_rel` VALUES (8, 3, 2, 'false');
INSERT INTO `rule_property_rel` VALUES (9, 4, 1, '2');
INSERT INTO `rule_property_rel` VALUES (10, 4, 2, 'false');

-- ----------------------------
-- Table structure for rule_scene_entity_rel
-- ----------------------------
DROP TABLE IF EXISTS `rule_scene_entity_rel`;
CREATE TABLE `rule_scene_entity_rel`  (
  `scene_entity_rel_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `scene_id` bigint(20) DEFAULT NULL COMMENT '场景',
  `entity_id` bigint(20) DEFAULT NULL COMMENT '实体',
  PRIMARY KEY (`scene_entity_rel_id`) USING BTREE,
  INDEX `scene_id`(`scene_id`) USING BTREE,
  INDEX `entity_id`(`entity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '场景实体关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_scene_entity_rel
-- ----------------------------
INSERT INTO `rule_scene_entity_rel` VALUES (3, 2, 2);
INSERT INTO `rule_scene_entity_rel` VALUES (4, 1, 1);

-- ----------------------------
-- Table structure for rule_scene_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_scene_info`;
CREATE TABLE `rule_scene_info`  (
  `scene_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `scene_identify` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标识',
  `scene_type` int(11) DEFAULT NULL COMMENT '类型(暂不使用)',
  `scene_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `scene_desc` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`scene_id`) USING BTREE,
  INDEX `scene_identify`(`scene_identify`) USING BTREE,
  INDEX `scene_type`(`scene_type`) USING BTREE,
  INDEX `scene_name`(`scene_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则引擎使用场景' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_scene_info
-- ----------------------------
INSERT INTO `rule_scene_info` VALUES (1, 'test', 1, '测试', '测试规则引擎', 1, 1, '2017-07-20 16:56:10', 'test');
INSERT INTO `rule_scene_info` VALUES (2, 'jifen', 1, '积分规则', '积分规则', 1, NULL, '2019-06-05 09:46:12', '');

-- ----------------------------
-- Table structure for rule_variable
-- ----------------------------
DROP TABLE IF EXISTS `rule_variable`;
CREATE TABLE `rule_variable`  (
  `variable_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `variable_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量名称',
  `variable_type` int(11) NOT NULL COMMENT '变量类型（1条件2动作）',
  `default_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '默认值',
  `value_type` int(11) NOT NULL COMMENT '数值类型（ 1字符型 2数字型 3 日期型）',
  `variable_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量值',
  `is_effect` int(11) NOT NULL DEFAULT 1 COMMENT '是否有效',
  `cre_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `cre_time` datetime(0) NOT NULL COMMENT '创建时间',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`variable_id`) USING BTREE,
  INDEX `variable_type`(`variable_type`) USING BTREE,
  INDEX `value_type`(`value_type`) USING BTREE,
  INDEX `variable_name`(`variable_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则引擎常用变量' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_variable
-- ----------------------------
INSERT INTO `rule_variable` VALUES (1, '增加积分100', 2, '100', 2, '100', 1, 1, '2017-07-20 18:38:01', NULL);
INSERT INTO `rule_variable` VALUES (2, '金额大于100', 1, '100', 2, '100', 1, 1, '2017-07-20 18:39:18', NULL);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":2,\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"qiniuBucketName\":\"ios-app\",\"aliyunDomain\":\"http://taskoss.oss-cn-beijing.aliyuncs.com\",\"aliyunPrefix\":\"taskoss\",\"aliyunEndPoint\":\"http://oss-cn-beijing.aliyuncs.com\",\"aliyunAccessKeyId\":\"LTAIFcrFM3x7NGLM\",\"aliyunAccessKeySecret\":\"8JweUDvRtYiwUoVKY6jkG705HXDi8k\",\"aliyunBucketName\":\"taskoss\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', 0, '云存储配置信息');
INSERT INTO `sys_config` VALUES (2, 'cwaHours', '4', 1, '半天的考勤小时数');
INSERT INTO `sys_config` VALUES (3, 'workHoursDayLegal', '8', 1, '法定每天工作小时数');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典码',
  `value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典值',
  `order_num` int(11) DEFAULT 0 COMMENT '排序',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父级ID',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) DEFAULT 0 COMMENT '删除标记  -1：已删除  0：正常',
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `creat_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type`(`type`, `code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 339 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '性别', 'sex', '0', '女', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (2, '性别', 'sex', '1', '男', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (3, '性别', 'sex', '2', '未知', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (4, '用户等级', 'level', '1', 'L1', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (5, '用户等级', 'level', '2', 'L2', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (6, '用户等级', 'level', '3', 'L3', 3, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (7, '用户等级', 'level', '4', 'L4', 4, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (8, '用户等级', 'level', '5', 'L5', 5, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (9, '动作类型', 'actionType', '1', '实现', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (10, '动作类型', 'actionType', '2', '自身', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (11, '是否有效', 'isEffect', '0', '无效', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (12, '是否有效', 'isEffect', '1', '有效', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (13, '数值类型', 'valueType', '1', '字符型', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (14, '数值类型', 'valueType', '2', '数字型', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (15, '数值类型', 'valueType', '3', '日期型', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (16, '变量类型', 'variableType', '1', '条件', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (17, '变量类型', 'variableType', '2', '动作', 2, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (18, '是否启用', 'ruleEnabled', '1', '启用', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (19, '是否启用', 'ruleEnabled', '2', '禁用', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (20, '门店状态', 'officeState', '0', '营运中', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (21, '门店状态', 'officeState', '1', '暂停营运', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (22, '门店状态', 'officeState', '2', '已停运', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (23, '门店状态', 'officeState', '3', '待营运', 3, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (24, '婚姻状态', 'wedlockType', '0', '未婚', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (25, '婚姻状态', 'wedlockType', '1', '已婚', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (26, '', 'nation', '1', '汉族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (27, '', 'nation', '10', '侗族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (28, '', 'nation', '11', '独龙族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (29, '', 'nation', '12', '傣族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (30, '', 'nation', '13', '俄罗斯族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (31, '', 'nation', '14', '鄂伦春族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (32, '', 'nation', '15', '鄂温克族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (33, '', 'nation', '16', '高山族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (34, '', 'nation', '17', '仡佬族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (35, '', 'nation', '18', '哈尼族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (36, '', 'nation', '19', '哈萨克族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (37, '', 'nation', '2', '阿昌族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (38, '', 'nation', '20', '汉族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (39, '', 'nation', '21', '赫哲族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (40, '', 'nation', '22', '回族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (41, '', 'nation', '23', '基诺族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (42, '', 'nation', '24', '京族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (43, '', 'nation', '25', '景颇族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (44, '', 'nation', '26', '柯尔克孜族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (45, '', 'nation', '27', '拉祜族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (46, '', 'nation', '28', '黎族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (47, '', 'nation', '29', '傈僳族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (48, '', 'nation', '3', '保安族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (49, '', 'nation', '30', '珞巴族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (50, '', 'nation', '31', '满族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (51, '', 'nation', '32', '毛南族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (52, '', 'nation', '33', '门巴族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (53, '', 'nation', '34', '蒙古族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (54, '', 'nation', '35', '苗族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (55, '', 'nation', '36', '仫佬族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (56, '', 'nation', '37', '纳西族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (57, '', 'nation', '38', '怒族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (58, '', 'nation', '39', '普米族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (59, '', 'nation', '4', '布朗族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (60, '', 'nation', '40', '羌族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (61, '', 'nation', '41', '撒拉族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (62, '', 'nation', '42', '畲族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (63, '', 'nation', '43', '水族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (64, '', 'nation', '44', '塔吉克族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (65, '', 'nation', '45', '塔塔尔族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (66, '', 'nation', '46', '土家族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (67, '', 'nation', '47', '土族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (68, '', 'nation', '48', '佤族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (69, '', 'nation', '49', '维吾尔族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (70, '', 'nation', '5', '布依族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (71, '', 'nation', '50', '乌兹别克族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (72, '', 'nation', '51', '锡伯族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (73, '', 'nation', '52', '瑶族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (74, '', 'nation', '53', '彝族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (75, '', 'nation', '54', '裕固族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (76, '', 'nation', '55', '藏族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (77, '', 'nation', '56', '壮族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (78, '', 'nation', '6', '朝鲜族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (79, '', 'nation', '7', '达斡尔族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (80, '', 'nation', '8', '德昂族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (81, '', 'nation', '9', '东乡族', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (89, '考勤类型', 'checkWorkAttendanceType', '0', '√', 1, NULL, '实际出勤', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (91, '考勤类型', 'checkWorkAttendanceType', '2', 'N', 3, NULL, '年假', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (92, '考勤类型', 'checkWorkAttendanceType', '3', 'T', 4, NULL, '调休', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (93, '考勤类型', 'checkWorkAttendanceType', '4', 'S', 5, NULL, '事假', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (94, '考勤类型', 'checkWorkAttendanceType', '5', 'B', 6, NULL, '病假', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (95, '考勤类型', 'checkWorkAttendanceType', '6', 'K', 7, NULL, '旷工', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (96, '籍贯', 'nativePlace', '0', '上海', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (97, '籍贯', 'nativePlace', '1', '江苏', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (98, '籍贯', 'nativePlace', '2', '浙江', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (99, '籍贯', 'nativePlace', '3', '安徽', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (100, '籍贯', 'nativePlace', '4', '北京', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (101, '籍贯', 'nativePlace', '5', '天津', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (102, '籍贯', 'nativePlace', '6', '广东', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (103, '籍贯', 'nativePlace', '7', '河北', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (104, '籍贯', 'nativePlace', '8', '河南', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (105, '籍贯', 'nativePlace', '9', '山东', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (106, '籍贯', 'nativePlace', '10', '湖北', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (107, '籍贯', 'nativePlace', '11', '湖南', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (108, '籍贯', 'nativePlace', '12', '江西', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (109, '籍贯', 'nativePlace', '13', '福建', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (110, '籍贯', 'nativePlace', '14', '四川', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (111, '籍贯', 'nativePlace', '15', '重庆', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (112, '籍贯', 'nativePlace', '16', '广西', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (113, '籍贯', 'nativePlace', '17', '山西', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (114, '籍贯', 'nativePlace', '18', '辽宁', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (115, '籍贯', 'nativePlace', '19', '吉林', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (116, '籍贯', 'nativePlace', '20', '黑龙江', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (117, '籍贯', 'nativePlace', '21', '贵州', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (118, '籍贯', 'nativePlace', '22', '陕西', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (119, '籍贯', 'nativePlace', '23', '云南', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (120, '籍贯', 'nativePlace', '24', '蒙古', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (121, '籍贯', 'nativePlace', '25', '甘肃', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (122, '籍贯', 'nativePlace', '26', '青海', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (123, '籍贯', 'nativePlace', '27', '宁夏', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (124, '籍贯', 'nativePlace', '28', '新疆', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (125, '籍贯', 'nativePlace', '29', '海南', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (126, '籍贯', 'nativePlace', '30', '西藏', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (127, '籍贯', 'nativePlace', '31', '香港', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (128, '籍贯', 'nativePlace', '32', '澳门', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (129, '籍贯', 'nativePlace', '33', '台湾', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (130, '政治面貌', 'politicId', '1', '中共党员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (131, '政治面貌', 'politicId', '2', '中共预备党员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (132, '政治面貌', 'politicId', '3', '共青团员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (133, '政治面貌', 'politicId', '4', '民革党员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (134, '政治面貌', 'politicId', '5', '民盟盟员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (135, '政治面貌', 'politicId', '6', '民建会员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (136, '政治面貌', 'politicId', '7', '民进会员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (137, '政治面貌', 'politicId', '8', '农工党党员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (138, '政治面貌', 'politicId', '9', '致公党党员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (139, '政治面貌', 'politicId', '10', '九三学社社员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (141, '政治面貌', 'politicId', '11', '台盟盟员', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (142, '政治面貌', 'politicId', '12', '无党派人士', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (143, '聘用形式', 'engageForm', '0', '正式工', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (144, '聘用形式', 'engageForm', '1', '临时工', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (145, '学历', 'tiptopDegree', '0', '小学', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (146, '学历', 'tiptopDegree', '1', '初中', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (147, '学历', 'tiptopDegree', '2', '高中', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (148, '学历', 'tiptopDegree', '3', '专科', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (149, '学历', 'tiptopDegree', '4', '本科', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (150, '学历', 'tiptopDegree', '5', '硕士研究生', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (151, '学历', 'tiptopDegree', '6', '博士研究生', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (152, '婚姻状态', 'wedlockType', '2', '离异', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (153, '在职状态', 'workState', '0', '在职', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (154, '在职状态', 'workState', '1', '离职', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (155, '证件类型', 'certificate_type', '1', '身份证', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (156, '', 'certificate_type', '10', '台湾通行证', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (157, '', 'certificate_type', '11', '外国人永久居留证', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (158, '', 'certificate_type', '12', '其他', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (159, '', 'certificate_type', '2', '护照', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (160, '', 'certificate_type', '3', '港澳通行证（双程证）', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (161, '', 'certificate_type', '4', '港澳来往内地通行证（回乡证）', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (162, '', 'certificate_type', '5', '台湾来往大陆通行证（台胞证）', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (163, '证件类型', 'certificate_type', '6', '户口簿', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (164, '', 'certificate_type', '7', '驾照', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (165, '', 'certificate_type', '8', '军人证', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (166, '', 'certificate_type', '9', '出生证', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (176, '时段', 'timeInterval', '0', '上午', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (177, '时段', 'timeInterval', '1', '下午', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (178, '考勤类型', 'checkWorkAttendanceType', '7', '休', 8, NULL, '休息', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (179, '考勤类型', 'checkWorkAttendanceType', '1', 'F', 2, NULL, '法定', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (180, '审核类型', 'auditType', '0', '离职', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (181, '审核类型', 'auditType', '1', '部门调动', 0, NULL, '0', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (182, '审核类型', 'auditType', '2', '调薪', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (183, '审核类型', 'auditType', '3', '奖罚', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (184, '审核状态', 'aduitStatus', '0', '待审核', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (185, '审核状态', 'aduitStatus', '1', '不通过', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (186, '审核状态', 'aduitStatus', '2', '通过', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (187, '', 'bank', '1', '中国工商银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (188, '', 'bank', '2', '招商银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (189, '', 'bank', '3', '中国农业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (190, '', 'bank', '4', '中国建设银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (191, '', 'bank', '5', '中国银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (192, '', 'bank', '6', '中国民生银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (193, '', 'bank', '7', '中国光大银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (194, '', 'bank', '8', '中信银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (195, '', 'bank', '9', '交通银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (196, '', 'bank', '10', '兴业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (197, '', 'bank', '11', '上海浦东发展银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (198, '', 'bank', '12', '中国人民银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (199, '', 'bank', '13', '华夏银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (200, '', 'bank', '14', '深圳发展银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (201, '', 'bank', '15', '广东发展银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (202, '', 'bank', '16', '国家开发银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (203, '', 'bank', '17', '厦门国际银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (204, '', 'bank', '18', '中国进出口银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (205, '', 'bank', '19', '中国农业发展银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (206, '', 'bank', '20', '北京银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (207, '', 'bank', '21', '上海银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (208, '', 'bank', '22', '中国邮政储蓄银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (209, '', 'bank', '23', '花旗中国银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (210, '', 'bank', '24', ' 汇丰中国银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (211, '', 'bank', '25', '渣打中国银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (212, '', 'bank', '26', '香港汇丰银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (213, '', 'bank', '27', '渣打(香港)银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (214, '', 'bank', '28', '美国银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (215, '', 'bank', '29', '东亚银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (216, '', 'bank', '30', '恒生银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (217, '', 'bank', '31', '华侨银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (218, '', 'bank', '33', ' 欧力士银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (219, '', 'bank', '34', '巴黎银行 ', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (220, '', 'bank', '35', '美国运通银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (221, '', 'bank', '36', '蒙特利尔银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (222, '', 'bank', '37', '包头市商业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (223, '', 'bank', '38', '北京农村商业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (224, '', 'bank', '39', '渤海银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (225, '', 'bank', '40', '常熟农村商业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (226, '', 'bank', '41', '大连银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (227, '', 'bank', '42', '德意志银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (228, '', 'bank', '43', '满地可银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (229, '', 'bank', '44', '长沙银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (230, '', 'bank', '45', '浙商银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (231, '', 'bank', '46', '郑州银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (232, '', 'bank', '47', '平安银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (233, '', 'bank', '48', '青岛市商业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (234, '', 'bank', '49', '瑞士银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (235, '', 'bank', '50', '厦门国际银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (236, '', 'bank', '51', '上海农村商业银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (237, '', 'bank', '52', '上海浦东发展银行', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (250, '', 'bank', '53', '南粤银行', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (251, '职称星级', 'joblevelId', '0', '初级', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (252, '职称星级', 'joblevelId', '1', '中级', 1, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (253, '职称星级', 'joblevelId', '2', '高级', 2, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (254, '职称星级', 'joblevelId', '3', '三星', 3, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (255, '职称星级', 'joblevelId', '4', '四星', 4, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (256, '职称星级', 'joblevelId', '5', '五星', 5, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (289, '', 'posId', '0', '见习总裁', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (290, '', 'posId', '1', '总经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (291, '', 'posId', '2', '教授', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (292, '', 'posId', '3', '总助', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (293, '', 'posId', '4', '主任', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (294, '', 'posId', '5', '人资经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (295, '', 'posId', '6', '人事主管', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (296, '', 'posId', '7', '薪酬专员', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (297, '', 'posId', '8', '行政前台', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (298, '', 'posId', '9', '人事专员', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (299, '', 'posId', '10', '店务总监', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (300, '', 'posId', '11', '招聘专员', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (301, '', 'posId', '12', '店务老师', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (302, '', 'posId', '13', '高级教务顾问', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (303, '', 'posId', '14', '教务助理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (304, '', 'posId', '15', '中级教务讲师', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (305, '', 'posId', '16', '招商部总监', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (306, '', 'posId', '17', '见习运营总监', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (307, '', 'posId', '18', '运营经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (308, '', 'posId', '19', '区域经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (309, '', 'posId', '20', '总监助理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (310, '', 'posId', '21', '策划总监', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (311, '', 'posId', '22', '新媒体运营', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (312, '', 'posId', '23', '网络运营', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (313, '', 'posId', '24', '运营推广经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (314, '', 'posId', '25', '设计师', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (315, '', 'posId', '26', '活动专员', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (316, '', 'posId', '27', '财务经理', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (317, '', 'posId', '28', '财务主管', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (318, '', 'posId', '29', '费用会计', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (319, '', 'posId', '30', '会计', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (320, '', 'posId', '31', '出纳', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (321, '组别', 'group', '0', '曾云组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (322, '组别', 'group', '1', '周红梅组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (323, '组别', 'group', '2', '韦明组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (324, '组别', 'group', '3', '肖梅丽', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (325, '组别', 'group', '4', '欧阳琪君组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (326, '分组', 'group', '5', '叶珊组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (327, '组别', 'group', '6', '张雅静组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (328, '组别', 'group', '7', '赵倩组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (329, '组别', 'group', '8', '吉燊组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (330, '组别', 'group', '9', '姚远组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (331, '组别', 'group', '10', '赵红丽组', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (332, '面子银行', 'faceBank', '0', '中国银行', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (333, '面子银行', 'faceBank', '1', '建设', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (334, '面子银行', 'faceBank', '2', '民生', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (335, '面子银行', 'faceBank', '3', '南粤', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (336, '职位', 'posId', '32', '肌肤管理师', 0, NULL, '', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES (338, '职位', 'posId', '33', '店长', 0, NULL, '', 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 411 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 144 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7);
INSERT INTO `sys_menu` VALUES (36, 1, '字典管理', 'sys/dict', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (37, 36, '查看', NULL, 'sys:dict:list,sys:dict:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (38, 36, '新增', NULL, 'sys:dict:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (39, 36, '修改', NULL, 'sys:dict:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (40, 36, '删除', NULL, 'sys:dict:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (139, 0, '规则配置', '', '', 0, 'mudedi', 5);
INSERT INTO `sys_menu` VALUES (140, 139, '规则实体信息', 'rules/entityinfo', '', 1, 'admin', 0);
INSERT INTO `sys_menu` VALUES (141, 139, '规则场景', 'rules/sceneinfo', '', 1, 'admin', 0);
INSERT INTO `sys_menu` VALUES (142, 139, '规则信息', 'rules/info', '', 1, 'admin', 0);
INSERT INTO `sys_menu` VALUES (143, 139, '规则动作', 'rules/actioninfo', '', 1, 'admin', 0);

-- ----------------------------
-- Table structure for sys_message_push
-- ----------------------------
DROP TABLE IF EXISTS `sys_message_push`;
CREATE TABLE `sys_message_push`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `push_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推送名称',
  `push_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推送内容',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `creat_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统消息推送' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `notice_title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告内容',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `creat_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统公告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES (1, 'http://taskoss.oss-cn-beijing.aliyuncs.com/taskoss/20190522/1bb5562fd8184e7899d0d8b120fd0b87.png', '2019-05-22 18:03:23');
INSERT INTO `sys_oss` VALUES (2, 'http://taskoss.oss-cn-beijing.aliyuncs.com/taskoss/20190522/416e15bb50db4150b5359ac9d20011f7.jpg', '2019-05-22 18:03:37');
INSERT INTO `sys_oss` VALUES (3, 'http://taskoss.oss-cn-beijing.aliyuncs.com/taskoss/20190629/511fb1df2cd849138135b38be3d7cc21.jpg', '2019-06-29 14:54:46');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `dept_id` bigint(20) DEFAULT 1 COMMENT '部门ID',
  `type` int(2) DEFAULT NULL COMMENT '0-门店 1-部门',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'abc', 1, '2019-04-26 13:39:27', 1, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3348 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (3303, 1, 1);
INSERT INTO `sys_role_menu` VALUES (3304, 1, 2);
INSERT INTO `sys_role_menu` VALUES (3305, 1, 15);
INSERT INTO `sys_role_menu` VALUES (3306, 1, 16);
INSERT INTO `sys_role_menu` VALUES (3307, 1, 17);
INSERT INTO `sys_role_menu` VALUES (3308, 1, 18);
INSERT INTO `sys_role_menu` VALUES (3309, 1, 3);
INSERT INTO `sys_role_menu` VALUES (3310, 1, 19);
INSERT INTO `sys_role_menu` VALUES (3311, 1, 20);
INSERT INTO `sys_role_menu` VALUES (3312, 1, 21);
INSERT INTO `sys_role_menu` VALUES (3313, 1, 22);
INSERT INTO `sys_role_menu` VALUES (3314, 1, 4);
INSERT INTO `sys_role_menu` VALUES (3315, 1, 23);
INSERT INTO `sys_role_menu` VALUES (3316, 1, 24);
INSERT INTO `sys_role_menu` VALUES (3317, 1, 25);
INSERT INTO `sys_role_menu` VALUES (3318, 1, 26);
INSERT INTO `sys_role_menu` VALUES (3329, 1, 27);
INSERT INTO `sys_role_menu` VALUES (3330, 1, 29);
INSERT INTO `sys_role_menu` VALUES (3337, 1, 36);
INSERT INTO `sys_role_menu` VALUES (3338, 1, 37);
INSERT INTO `sys_role_menu` VALUES (3339, 1, 38);
INSERT INTO `sys_role_menu` VALUES (3340, 1, 39);
INSERT INTO `sys_role_menu` VALUES (3341, 1, 40);
INSERT INTO `sys_role_menu` VALUES (3342, 1, 139);
INSERT INTO `sys_role_menu` VALUES (3343, 1, 140);
INSERT INTO `sys_role_menu` VALUES (3344, 1, 141);
INSERT INTO `sys_role_menu` VALUES (3345, 1, 142);
INSERT INTO `sys_role_menu` VALUES (3346, 1, 143);
INSERT INTO `sys_role_menu` VALUES (3347, 1, -666666);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `first` tinyint(4) DEFAULT 1 COMMENT ' 1：第一次   0：不是第一次',
  `dept_id` bigint(20) DEFAULT 1 COMMENT '部门ID',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `move_in_brand` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`, `mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@163.com', '13612345678', 1, 0, 1, 1, '2016-11-11 11:11:11', NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 2, 1);
INSERT INTO `sys_user_role` VALUES (2, 3, 1);
INSERT INTO `sys_user_role` VALUES (18, 1, 1);
INSERT INTO `sys_user_role` VALUES (20, 6, 1);
INSERT INTO `sys_user_role` VALUES (34, 7, 1);
INSERT INTO `sys_user_role` VALUES (57, 5, 1);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (5, '03e9e7c9f9d3a1bb09607fe297c3d302', '2019-06-26 21:08:48', '2019-06-26 09:08:48');
INSERT INTO `sys_user_token` VALUES (5, '2135a544b8c6f3b97dde7b76927f2ada', '2019-06-26 06:38:17', '2019-06-25 18:38:17');
INSERT INTO `sys_user_token` VALUES (4, '215065dd685ea990c604e2510a316931', '2019-06-30 06:18:48', '2019-06-29 18:18:48');
INSERT INTO `sys_user_token` VALUES (6, '26a30889904ab5955e2058e15e751bc6', '2019-07-12 22:46:42', '2019-07-12 10:46:42');
INSERT INTO `sys_user_token` VALUES (6, '3142b5cb658c68628ecc877558babe6d', '2019-07-12 01:37:05', '2019-07-11 13:37:05');
INSERT INTO `sys_user_token` VALUES (2, '3ee8d040f91ebd9051899eceb6380649', '2019-04-27 02:32:40', '2019-04-26 14:32:40');
INSERT INTO `sys_user_token` VALUES (6, '4f16438d0cb00cb374aaf24cd749c9cf', '2019-07-11 21:57:22', '2019-07-11 09:57:22');
INSERT INTO `sys_user_token` VALUES (6, '52ff084cc405f377f28996de363df1b1', '2019-07-11 23:37:25', '2019-07-11 11:37:25');
INSERT INTO `sys_user_token` VALUES (5, '6555a223cfaba5dd2e1edf99a255535f', '2019-06-26 06:31:32', '2019-06-25 18:31:32');
INSERT INTO `sys_user_token` VALUES (5, '8023d159963d25a152eb31e1754fe154', '2019-06-27 02:51:11', '2019-06-26 14:51:11');
INSERT INTO `sys_user_token` VALUES (5, 'ac6a3209546f4348d56d7ba9130066cc', '2019-06-26 01:43:58', '2019-06-25 13:43:58');
INSERT INTO `sys_user_token` VALUES (15, 'c4ea0d720bbf74491771f61350b66454', '2019-07-12 06:23:18', '2019-07-11 18:23:18');
INSERT INTO `sys_user_token` VALUES (6, 'cd334c2c2c20a8e2101bdafb7df6e467', '2019-07-11 22:08:57', '2019-07-11 10:08:57');
INSERT INTO `sys_user_token` VALUES (6, 'cf4e37a0df5b8263700d297d770a9783', '2019-07-12 21:04:05', '2019-07-12 09:04:05');
INSERT INTO `sys_user_token` VALUES (1, 'df3ea65241caa9e4ea997ed3cc800ae4', '2020-05-14 06:11:09', '2020-05-13 18:11:09');
INSERT INTO `sys_user_token` VALUES (5, 'f292e5804a2e19c9895990ee289289d3', '2019-06-25 21:06:09', '2019-06-24 09:06:09');
INSERT INTO `sys_user_token` VALUES (5, 'f69db6cadfb0d7d4f8c05044547973f0', '2019-06-26 02:08:35', '2019-06-25 14:08:35');
INSERT INTO `sys_user_token` VALUES (5, 'fa3c55f4462235e6b9a8ca605e6b5abb', '2019-06-26 06:06:08', '2019-06-25 18:06:08');
INSERT INTO `sys_user_token` VALUES (5, 'ff8e2dce1caf18ab69bc388475a80499', '2019-06-26 00:57:32', '2019-06-25 12:57:32');

SET FOREIGN_KEY_CHECKS = 1;
