package com.rules.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.admin.entity.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {

}
