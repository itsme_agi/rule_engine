package com.rules.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.VariableDao;
import com.rules.drools.entity.VariableEntity;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("variableService")
public class VariableService extends ServiceImpl<VariableDao, VariableEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<VariableEntity> page = this.page(
                new Query<VariableEntity>().getPage(params),
                new QueryWrapper<VariableEntity>()
        );

        return new PageUtils(page);
    }

}
