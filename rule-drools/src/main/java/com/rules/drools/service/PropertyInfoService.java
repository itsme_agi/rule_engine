package com.rules.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rules.core.utils.CollectionUtil;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.core.utils.Result;
import com.rules.drools.dao.PropertyInfoDao;
import com.rules.drools.entity.PropertyInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("propertyInfoService")
public class PropertyInfoService extends ServiceImpl<PropertyInfoDao, PropertyInfoEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PropertyInfoEntity> page = this.page(
                new Query<PropertyInfoEntity>().getPage(params),
                new QueryWrapper<PropertyInfoEntity>()
        );

        return new PageUtils(page);
    }

    public Result listProperty(){
        QueryWrapper<PropertyInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("is_effect",1);
        List<PropertyInfoEntity> list = this.list(wrapper);
        if(CollectionUtil.collectionIsNull(list)){
            return Result.ok();
        }
        List<Map<String,Object>> res = Lists.newArrayList();
        for (PropertyInfoEntity entity : list) {
            Map<String,Object> map = Maps.newHashMap();
            map.put("id",entity.getRulePropertyId());
            map.put("identifyName",entity.getRulePropertyIdentify()+"("+entity.getRulePropertyName()+")");
            map.put("identify",entity.getRulePropertyIdentify());
            res.add(map);
        }
        return Result.ok().put("data",res);
    }



}
