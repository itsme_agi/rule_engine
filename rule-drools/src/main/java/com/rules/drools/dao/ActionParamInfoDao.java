package com.rules.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.drools.entity.ActionParamInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 动作参数信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface ActionParamInfoDao extends BaseMapper<ActionParamInfoEntity> {


    /**
     *
     * 方法说明: 获取动作参数信息
     * @param baseRuleActionParamInfo 参数
     */
    List<ActionParamInfoEntity> findBaseRuleActionParamInfoList(ActionParamInfoEntity baseRuleActionParamInfo);

    /**
     *
     * 方法说明: 根据动作id获取动作参数信息
     * @param actionId 参数
     */
    List<ActionParamInfoEntity> findRuleActionParamByActionId(@Param("actionId") Long actionId);

}
