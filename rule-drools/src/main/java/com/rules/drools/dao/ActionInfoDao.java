package com.rules.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rules.drools.entity.ActionInfoEntity;
import com.rules.drools.entity.SceneInfoEntity;
import com.rules.drools.vo.req.ActionInfoReqVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 规则动作定义信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface ActionInfoDao extends BaseMapper<ActionInfoEntity> {

    /**
     * <p>
     * 方法说明: 获取动作列表
     *
     * @param baseRuleActionInfo 参数
     */
    List<ActionInfoEntity> findBaseRuleActionInfoList(ActionInfoEntity baseRuleActionInfo);

    /**
     * <p>
     * 方法说明: 根据场景获取所有的动作信息
     *
     * @param sceneInfo 参数
     */
    List<ActionInfoEntity> findRuleActionListByScene(SceneInfoEntity sceneInfo);


    /**
     * <p>
     * 方法说明: 根据规则id获取动作集合
     *
     * @param ruleId 参数
     */
    List<ActionInfoEntity> findRuleActionListByRule(@Param("ruleId") Long ruleId);

    /**
     * <p>
     * 方法说明: 查询是否有实现类的动作
     *
     * @param ruleId 规则id
     */
    Integer findRuleActionCountByRuleIdAndActionType(@Param("ruleId") Long ruleId);

    /**
     * 通过动作id获取详情
     * @param actionId
     * @return
     */
    ActionInfoReqVo getOne(Long actionId);

    IPage<ActionInfoReqVo> pageAction(IPage<ActionInfoReqVo> page);
}
