package com.rules.drools.vo.req;

import com.rules.drools.entity.ConditionInfoEntity;
import com.rules.drools.vo.PropertyRelVo;
import lombok.Data;

import java.util.List;

/**
 * ClassName: RulesInfoEntityReqVo <br/>
 * Description: 规则实体<br/>
 * date: 2019/7/12 17:52<br/>
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class RulesInfoEntityReqVo {

    private Long ruleId;
    /**
     * 场景
     */
    private Long sceneId;
    /**
     * 名称
     */
    private String ruleName;
    /**
     * 描述
     */
    private String ruleDesc;
    /**
     * 是否启用
     */
    private Integer ruleEnabled;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 备注
     */
    private String remark;
    /**
     * 规则条件
     */
    private List<ConditionInfoEntity> conditionInfoItem;
    /**
     * 规则属性
     */
    private List<PropertyRelVo> propertyInfoItem;
    /**
     * 规则动作
     */
    private List<Long> actionRuleRelItem;

}
