package com.rules.drools.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;

    //创建人
    private Long creUserId;
    //创建时间
    private Date creTime;
    //是否有效
    private Integer isEffect;
    //备注
    private String remark;
}
