package com.rules.drools.vo.req;

import lombok.Data;
import org.dozer.Mapping;

import java.util.List;

/**
 * ClassName: RulesEntityReqVo <br/>
 * Description: <br/>
 * date: 2019/7/12 14:00<br/>
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
public class RulesEntityReqVo {
    @Mapping(value = "entityId")
    private Long id;
    /**
     * 名称
     */
    private String entityName;
    /**
     * 描述
     */
    private String entityDesc;
    /**
     * 标识
     */
    private String entityIdentify;
    /**
     * 包路径
     */
    private String pkgName;
    /**
     * 是否有效(1是0否)
     */
    private String isEffect;
    /**
     * 备注
     */
    private String remark;
    /**
     * 关联字段
     */
    private List<RuleEntityItemReqVo> item;
    /**
     * 字段串
     */
    private String itemStr;
    /**
     * 场景
     */
    private String scene;

}
