package com.rules.drools.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PropertyRelVo implements Serializable {
   /* private Long ruleProRelId;
    private Long ruleId;*/
    /**
     * 规则属性ID
     */
   private Long rulePropertyId;

    /**
     * 规则属性值
     */
    private String rulePropertyValue;

    /**
     * 标识
     */
    private String rulePropertyIdentify;
    /**
     * 名称
     */
    private String rulePropertyName;
    /**
     * 描述
     */
    private String rulePropertyDesc;
    /**
     * 默认值
     */
    private String defaultValue;
    /**
     * 是否有效
     */
    private Integer isEffect;
    /**
     * 备注
     */
    private String remark;

}
