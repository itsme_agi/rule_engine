package com.rules.drools.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * ClassName: OrderRule <br/>
 * Description: <br/>
 * date: 2019/6/5 14:18<br/>
 *
 * @author gz<br />
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderRule extends BaseModel {
    // 订单id
    private String orderId;
    // 会员id
    private String memberId;
    // 订单金额
    private Double orderMoney;
    // 会员已有总积分
    private Double beforeScore;
    // 当前订单计算后的总积分
    private Double afterScore;
    // 会员当前订单产生的积分
    private Double nowScore;

}
