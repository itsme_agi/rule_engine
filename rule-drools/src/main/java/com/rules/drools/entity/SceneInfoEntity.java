package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则引擎使用场景
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_scene_info")
public class SceneInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long sceneId;
	/**
	 * 标识
	 */
	private String sceneIdentify;
	/**
	 * 类型(暂不使用)
	 */
	private Integer sceneType;
	/**
	 * 名称
	 */
	private String sceneName;
	/**
	 * 描述
	 */
	private String sceneDesc;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
