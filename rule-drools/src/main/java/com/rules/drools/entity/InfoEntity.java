package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则信息
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_info")
public class InfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long ruleId;
	/**
	 * 场景
	 */
	private Long sceneId;
	/**
	 * 名称
	 */
	private String ruleName;
	/**
	 * 描述
	 */
	private String ruleDesc;
	/**
	 * 是否启用
	 */
	private Integer ruleEnabled;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
